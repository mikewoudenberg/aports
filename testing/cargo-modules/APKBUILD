# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=cargo-modules
pkgver=0.10.3
pkgrel=0
pkgdesc="A cargo plugin for showing an overview of a crate's modules"
url="https://github.com/regexident/cargo-modules"
# s390x: FTBFS
arch="all !s390x"
license="MPL-2.0"
makedepends="
	cargo
	cargo-auditable
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/regexident/cargo-modules/archive/$pkgver.tar.gz"
options="net" # needed to fetch crates

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-modules -t "$pkgdir"/usr/bin
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
}

sha512sums="
df34b9d580183354f46e8359dbd6ac7be6e89e810987fabcb410bbece6de264cd78a22c8577e32993494ee5c2de768f3b5e73e2dbe4c41c548f8513d7d7c4f91  cargo-modules-0.10.3.tar.gz
"
